package com.skillversum.springseculogindemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.skillversum.springseculogindemo")
public class SpringseculogindemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringseculogindemoApplication.class, args);
    }
}
