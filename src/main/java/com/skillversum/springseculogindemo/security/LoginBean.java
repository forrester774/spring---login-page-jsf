package com.skillversum.springseculogindemo.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;

@Named
public class LoginBean {

    public void onPageLoad(){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/index.xhtml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
