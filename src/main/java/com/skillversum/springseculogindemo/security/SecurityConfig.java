package com.skillversum.springseculogindemo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/javax.faces.resource/**", "/static").permitAll().anyRequest()
                .authenticated();


        http.formLogin().loginPage("/login.xhtml").permitAll().failureUrl("/login.xhtml?failed=1")
                .successForwardUrl("/index.xhtml");


        http.logout().logoutUrl("/logout").logoutSuccessUrl("/login.xhtml");
        http.csrf().disable();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.inMemoryAuthentication().withUser("User1")
                .password("1234").roles("USER").and().withUser("User2")
                .password("5678").roles("ADMIN");
    }
}
